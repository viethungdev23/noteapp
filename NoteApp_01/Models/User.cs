﻿using System.Net.NetworkInformation;

namespace NoteApp_01.Models;

public class User
{
    public User() 
    {
        this.UserRoles = new HashSet<UserRole>();
    }
    public int UserId { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string AvataUrl { get; set; }
    public DateTime CreateDate { get; set;}

    public virtual ICollection<UserRole>? UserRoles { get; set; }
    public virtual ICollection<Note>? Notes { get; set; }
}
