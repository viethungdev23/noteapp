﻿namespace NoteApp_01.Models;

public class Note
{
    public int NoteId { get; set; }
    public string Content { get; set; }
    public DateTime CreatedDate { get; set; } = DateTime.Now;
    public DateTime? UpdatedDate { get; set;}
    public int UserId { get; set; }


    public virtual User? User { get; set; }
}
