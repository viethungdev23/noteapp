﻿using NoteApp_01.Data;
using NoteApp_01.GenericRepository;
using NoteApp_01.Models;

namespace NoteApp_01.Repositories;
public interface IUserRoleRepository : IGenericRepository<UserRole>
{

}
public class UserRoleRepository : GenericRepository<UserRole>, IUserRoleRepository 
{
	public UserRoleRepository(NoteContext context) : base(context)
	{

	}
}
