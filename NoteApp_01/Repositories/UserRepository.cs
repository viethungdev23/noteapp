﻿using NoteApp_01.Data;
using NoteApp_01.GenericRepository;
using NoteApp_01.Models;

namespace NoteApp_01.Repositories;
public interface IUserRepository : IGenericRepository<User>
{
     // add some function 
}
public class UserRepository : GenericRepository<User>,IUserRepository
{
	public UserRepository(NoteContext context) : base(context)
    {

	}
}
