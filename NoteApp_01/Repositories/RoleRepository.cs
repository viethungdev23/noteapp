﻿using NoteApp_01.Data;
using NoteApp_01.GenericRepository;
using NoteApp_01.Models;

namespace NoteApp_01.Repositories;

public interface IRoleRepository : IGenericRepository<Role>
{

}
public class RoleRepository : GenericRepository<Role>, IRoleRepository
{
	public RoleRepository(NoteContext context) : base(context)	
	{

	}
}
