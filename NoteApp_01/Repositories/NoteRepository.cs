﻿using AutoMapper;
using NoteApp_01.Data;
using NoteApp_01.GenericRepository;
using NoteApp_01.Models;
using System.Linq;

namespace NoteApp_01.Repositories;

public interface INoteRepository : IGenericRepository<Note>
{
   
}
public class NoteRepository : GenericRepository<Note>, INoteRepository
{
	public NoteRepository(NoteContext context) : base(context)
	{

	}

  
}
