﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NoteApp_01.Data.Infrastructure;
using NoteApp_01.Models;

namespace NoteApp_01.Controllers;
[Route("api/[controller]")]
[ApiController]
public class UserController : Controller
{
    private readonly IUnitOfWork _unitOfWork;

    public UserController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public IActionResult getAllUser()
    {
        var users = _unitOfWork.User.GetAll();
        return Ok(users);
    }

    [HttpGet("{id}")]
    public IActionResult getUser(int id)
    {
        var user = _unitOfWork.User.GetById(id);
        return Ok(user);
    }

    [HttpPut]
    public IActionResult UpdateUser(User user)
    {
        if (ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }
        _unitOfWork.User.Update(user);
        return Ok(user);
    }

    [HttpPost]
    public IActionResult AddUser(User user)
    {
        _unitOfWork.User.Add(user);
        return Ok(user);
    }
}
