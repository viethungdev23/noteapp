﻿using Microsoft.AspNetCore.Mvc;
using NoteApp_01.Data.Infrastructure;
using NoteApp_01.Dtos;
using NoteApp_01.Models;
using NoteApp_01.Services;

namespace NoteApp_01.Controllers;

[Route("api/[controller]")]
[ApiController]
public class NoteController : Controller
{
    private readonly INoteService _noteSevice;
    public NoteController(INoteService noteService )
    {
        _noteSevice = noteService;
    }

    [HttpGet]
    public IActionResult getAllNote()
    {
        var notes = _noteSevice.GetAll();
        return Ok(notes);
    }

    [HttpGet("{id}")]
    public IActionResult getNote(int id) 
    {
        var note = _noteSevice.GetById(id);
        return Ok(note);
    }

    [HttpPut]
    public IActionResult UpdateNote(NoteDTO note)
    {
        if(!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }
        _noteSevice.Update(note);
        return Ok(note);
    }

    [HttpPost]
    public IActionResult AddNote(NoteDTO note)
    {
        _noteSevice.Add(note);
       // _noteSevice.SaveChanges();
        return Ok(note);
    }
}
