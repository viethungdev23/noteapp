﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NoteApp_01.Data.Infrastructure;
using NoteApp_01.Models;

namespace NoteApp_01.Controllers;

[Route("api/[controller]")]
[ApiController]
public class RoleController : Controller
{
    private readonly IUnitOfWork _unitOfWork;

    public RoleController(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    [HttpGet]
    public IActionResult getAllRole()
    {
        var roles = _unitOfWork.Role.GetAll();
        return Ok(roles);
    }

    [HttpGet("{id}")]
    public IActionResult getRole(int id)
    {
        var role = _unitOfWork.Role.GetById(id);
        return Ok(role);
    }

    [HttpPut]
    public IActionResult UpdateRole(Role role)
    {
        if (ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }
        _unitOfWork.Role.Update(role);
        return Ok(role);
    }

    [HttpPost]
    public IActionResult AddRole(Role role)
    {
        _unitOfWork.Role.Add(role);
        return Ok(role);
    }
}
