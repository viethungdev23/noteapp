﻿using AutoMapper;
using NoteApp_01.Data.Infrastructure;
using NoteApp_01.Dtos;
using NoteApp_01.Models;
using NoteApp_01.Repositories;

namespace NoteApp_01.Services;

public interface INoteService
{
    void Add(NoteDTO noteDto);
    void Update(NoteDTO noteDto);
    void Delete(int id);
    IEnumerable<NoteDTO> GetAll();
    Note GetById(int id);
    void SaveChanges();

}
public class NoteSevice : INoteService
{
    INoteRepository _noteRepository;
    IUnitOfWork _unitOfWork;
    IMapper _mapper;
    public NoteSevice(INoteRepository noteRepository, IUnitOfWork unitOfWork,IMapper mapper)
    {
        _noteRepository= noteRepository;
        _unitOfWork= unitOfWork;
        _mapper= mapper;
    }
    public void Add(NoteDTO noteDto)
    {
        var entity = _mapper.Map<Note>(noteDto);
        _noteRepository.Add(entity);
    }

    public void Delete(int id)
    {
        _noteRepository.DeleteById(id);
    }

    public IEnumerable<NoteDTO> GetAll()
    {
        var dto = _mapper.Map<IEnumerable<NoteDTO>>(_noteRepository.GetAll());
        return dto;
    }

    public Note GetById(int id)
    {
        
        return _noteRepository.GetById(id);
    }

    public void SaveChanges()
    {
        _unitOfWork.Save();
    }

    public void Update(NoteDTO noteDto)
    {
        var entity = _mapper.Map<Note>(noteDto);
        _noteRepository.Update(entity);
    }
}
