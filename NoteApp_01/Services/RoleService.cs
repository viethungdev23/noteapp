﻿using NoteApp_01.Data.Infrastructure;
using NoteApp_01.Models;
using NoteApp_01.Repositories;

namespace NoteApp_01.Services;

public interface IRoleService
{
    void Add(Role role);
    IEnumerable<Role> GetRoles();
    void Update(Role role);
}
public class RoleService : IRoleService
{
    IRoleRepository _roleRepository;
    IUnitOfWork _unitOfWork;

    public RoleService(IRoleRepository roleRepository, IUnitOfWork unitOfWork)
    {
        _roleRepository = roleRepository;
        _unitOfWork = unitOfWork;
    }

    public void Add(Role role)
    {
        _roleRepository.Add(role);
    }

    public IEnumerable<Role> GetRoles()
    {
        return _roleRepository.GetAll();
    }

    public void Update(Role role)
    {
        _roleRepository.Update(role);
    }
}
