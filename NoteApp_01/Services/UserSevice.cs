﻿using NoteApp_01.Data.Infrastructure;
using NoteApp_01.Models;
using NoteApp_01.Repositories;

namespace NoteApp_01.Services;

public interface IUserSvice
{
    void Add(User user);
    void Update(User user);
    IEnumerable<User> GetAll();
    User GetById(int id);
}
public class UserSevice : IUserSvice
{
    IUserRepository _userRepository;
    IUnitOfWork _unitOfWork;

    public UserSevice(IUserRepository userRepository, IUnitOfWork unitOfWork)
    {
        _userRepository = userRepository;
        _unitOfWork = unitOfWork;
    }

    public void Add(User user)
    {
        _userRepository.Add(user);
    }

    public IEnumerable<User> GetAll()
    {
        return _userRepository.GetAll();
    }

    public User GetById(int id)
    {
        return _userRepository.GetById(id);
    }

    public void Update(User user)
    {
        _userRepository.Update(user);
    }
}
