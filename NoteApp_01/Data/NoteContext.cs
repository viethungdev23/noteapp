﻿using Microsoft.EntityFrameworkCore;
using NoteApp_01.Models;

namespace NoteApp_01.Data
{
    public class NoteContext : DbContext
    {
        public NoteContext(DbContextOptions<NoteContext> options) :base(options) { }

        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<Note> Notes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>(entity =>
            {
                entity
                .ToTable("user")
                .HasKey(e => e.UserId);

                entity
                .Property(e => e.UserId)
                .UseIdentityColumn(1, 1);
            });


            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.RoleId).HasMaxLength(50);
                entity.Property(e => e.RoleName).HasMaxLength(20);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasOne(e => e.Role)
                .WithMany(d => d.UserRoles)
                .HasForeignKey(a => a.RoleId);

                entity.HasOne(e => e.User)
                .WithMany(d => d.UserRoles)
                .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Note>(entity =>
            {
                entity.HasOne<User>(u => u.User)
                .WithMany(d => d.Notes)
                .HasForeignKey(s => s.UserId);


            });

            modelBuilder.Entity<Note>().HasData(
                new Note { Content = "hahahaha ha" , CreatedDate= new DateTime(), UpdatedDate= new DateTime(), NoteId = 1, UserId = 1 },
                new Note { Content = "hehe hehe", CreatedDate = new DateTime(), UpdatedDate = new DateTime(), NoteId = 2, UserId = 2}
                );
            modelBuilder.Entity<User>().HasData(
                    new User { UserId = 1, AvataUrl = "avata", CreateDate= new DateTime(), Email = "a@gmail.com", Name = "dan choi", Password = "hehehe"  },
                    new User { UserId = 2, AvataUrl = "avata2", CreateDate = new DateTime(), Email = "a2@gmail.com", Name = "dan choi2", Password = "hehehe2" }
                );
            modelBuilder.Entity<Role>().HasData(
                    new Role { RoleId = 1, RoleName = "USER"},
                    new Role { RoleId = 2, RoleName = "ADMIN" }
                );
            modelBuilder.Entity<UserRole>().HasData(
                    new UserRole { Id = 1 , RoleId = 1, UserId = 1},
                    new UserRole { Id = 2, RoleId = 2, UserId = 2 }
                );
        }
    }
}
