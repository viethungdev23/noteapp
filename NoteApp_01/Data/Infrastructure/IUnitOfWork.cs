﻿using NoteApp_01.GenericRepository;
using NoteApp_01.Models;
using NoteApp_01.Repositories;

namespace NoteApp_01.Data.Infrastructure;

public interface IUnitOfWork : IDisposable
{
    /*
    IGenericRepository<Note> Notes { get; }
    IGenericRepository<User> Users { get; }
    IGenericRepository<Role> Roles { get; }

    Task Save();
    */

    INoteRepository Note { get; }
    IRoleRepository Role { get; }
    IUserRepository User { get; }
    IUserRoleRepository UserRole { get; }

    int Save();
}
