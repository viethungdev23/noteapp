﻿using NoteApp_01.GenericRepository;
using NoteApp_01.Models;
using NoteApp_01.Repositories;

namespace NoteApp_01.Data.Infrastructure;

public class UnitOfWork : IUnitOfWork
{
    private readonly NoteContext _context;

    public UnitOfWork(NoteContext context)
    {
        _context = context;
        User = new UserRepository(_context);
        Role = new RoleRepository(_context);
        Note = new NoteRepository(_context);
        UserRole = new UserRoleRepository(_context);
    }
    
    public IUserRepository User { get; private set; }
    public IRoleRepository Role { get; private set; }
    public INoteRepository Note { get; private set; }
    public IUserRoleRepository UserRole { get; private set; }

    
    public void Dispose()
    {
        _context.Dispose();
        GC.SuppressFinalize(this);
    }

    public int Save()
    {
        return _context.SaveChanges();
    }
}
