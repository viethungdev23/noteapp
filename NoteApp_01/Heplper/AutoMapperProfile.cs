﻿using AutoMapper;
using NoteApp_01.Dtos;
using NoteApp_01.Models;

namespace NoteApp_01.Heplper;

public class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<Note,NoteDTO>().ReverseMap();
        CreateMap<User,UserDTO>().ReverseMap();
        CreateMap<Role,RoleDTO>().ReverseMap();
    }
}
