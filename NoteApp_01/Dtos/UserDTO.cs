﻿namespace NoteApp_01.Dtos;

    public class UserDTO
    {
    public int UserId { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public string AvataUrl { get; set; }
    public DateTime CreateDate { get; set; } 
}

