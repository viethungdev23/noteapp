﻿namespace NoteApp_01.Dtos;

public class NoteDTO
{
    public int NoteId { get; set; }
    public string Content { get; set; }
    public DateTime CreatedDate { get; set; } 
    public DateTime? UpdatedDate { get; set; }
}
