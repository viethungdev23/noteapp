﻿namespace NoteApp_01.GenericRepository;

public interface IGenericRepository<T> where T : class
{
    IEnumerable<T> GetAll(string[] includes = null);
    void Add(T entity);
    T GetById(int id);
    void Update(T entity);
    void DeleteById(int id);
}
