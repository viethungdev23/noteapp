﻿using Microsoft.EntityFrameworkCore;
using NoteApp_01.Data;

namespace NoteApp_01.GenericRepository;

public abstract class GenericRepository<T> : IGenericRepository<T> where T : class
{
    internal NoteContext _context;
    internal DbSet<T> _dbSet;

    // inject 
    public GenericRepository(NoteContext context)
    {
        _context = context;
        _dbSet = context.Set<T>();
    }
    // ADD function
    public void Add(T entity)
    {
         _dbSet.AddAsync(entity);
    }
    // Delete Function
    public void DeleteById(int id)
    {
        var entity = _dbSet.Find(id);
        _dbSet.Remove(entity);
    }
    // Get All
    public IEnumerable<T> GetAll(string[] includes = null)
    {
        //HANDLE INCLUDES FOR ASSOCIATED OBJECTS IF APPLICABLE
        if (includes != null && includes.Count() > 0)
        {
            var query = _dbSet.Include(includes.First());
            foreach (var include in includes.Skip(1))
                query = query.Include(include);
            return query.AsQueryable();
        }
        return  _dbSet.AsQueryable();
    }
    // Get by id
    public T GetById(int id)
    {
        return  _dbSet.Find(id);
    }
    // Update Function
    public void Update(T entity)
    {
        _dbSet.Attach(entity);
        _context.Entry(entity).State = EntityState.Modified;
    }
}
